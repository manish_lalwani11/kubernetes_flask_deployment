FROM python:3.8.13-alpine
WORKDIR /inside_docker
ADD . /inside_docker
RUN pip install -r requirements.txt
CMD  ["python","apps.py"]